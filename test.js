const number = [1, 2, 3, 4, 5]
console.log(number);

//index
const number1 = [1, 2, 3, 4, 5]
console.log(number1[2])

//add element 
const furits = ["mango", "apple", "organge"]
furits[3] = "papya"
console.log(furits)

//push
furits.push("banana") //add element to end of array
console.log(furits)

//unshift()
furits.unshift("1apple") // add elements begining to array 
console.log(furits)

//splice() 
furits.splice(3, 6, "grapes", "gavuva") // add and remove from array 
console.log(furits)

//indexof()
console.log(furits.indexOf("apple")) // search array and returns its positions

//lastindexof()
console.log(furits.lastIndexOf("mango")) //search array ,starting at end , return poistions

//includes()
console.log(furits.includes("mango")) //check array contains specified true / false

//find()
const array1 = [1, 12, 8, 130, 44];
const test = array1.find(element => element > 10);
console.log(test); //  return value of first element that pass test

//findindexof()
const array2 = [5, 12, 8, 130, 44];
const isLargeNumber = (element) => element > 13;
console.log(array2.findIndex(isLargeNumber));

//filter()
const names = ['suriya', 'jaganraj', 'jagadesh', 'abilash', 'suriyakarunindhi'];
const result = names.filter(names => names.length > 6);
console.log(result) // create a new array with pass element 

//fill()
const test2 = [1, 2, 3, 4];
console.log(test2.fill(0, 2, 4));
console.log(test2.fill(5, 1));
console.log(test2.fill(6)); // fill the elements in array with static value 

//map()
const numbers = [12, 55, 3, 28, 99];
let result1 = numbers.map((value) => {
    return value > 50;
})
console.log(result1); //creates a new array with result of a calling function each array element

//reduce()
const array3 = [1, 2, 3, 4];
const initialValue = 0;
const sumWithInitial = array3.reduce(
    (previousValue, currentValue) => previousValue + currentValue,
    initialValue
);
console.log(sumWithInitial); //reduces value of an array to sigle vaue ( L to R )

//pop
const plants = ['onion', 'wheat', 'cabbage', 'beans', 'tomato'];
console.log(plants.pop()); // removes last element of an array , returns that element 

//shift
const array4 = [1, 2, 3];
const firstElement = array4.shift();
console.log(firstElement);//removes first element of array,
console.log(array4); //  removes first element of array, and return element

//splice

const months = ['Jan', 'March', 'April', 'June'];
months.splice(1, 0, 'Feb'); 
console.log(months); //  add elements frm array
months.splice(4, 1, 'May');
console.log(months); // removes from array 